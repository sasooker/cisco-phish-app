import * as request from 'supertest';
import '../src/lib/db';
import Phishes from '../src/models/Phishes';
import {server} from '../src/server';

const date = new Date('2018-04-05T05:00:00Z');
const url = 'testarooni';
let phish: Phishes;

beforeEach(() => {
    phish = Phishes.build({id: 1, url, verifiedAt: date});
    phish.save();
});

afterEach(() => {
    phish.destroy();
    server.close();
});

test('should 404 on non matching route', async () => {
    const response = await request(server).get('/non-matching');
    expect(response.status).toBe(404);
    expect(response.body).toEqual({message: 'not found'});
});

test('should 404 when a URL is not a phish', async () => {
    const response = await request(server).get('/verify?url=non-matching');
    expect(response.status).toBe(404);
    expect(response.body).toEqual({message: 'not found'});
});

test('should return the phish information for a URL that is detected as a phish', async () => {
    const response = await request(server).get(`/verify?url=${url}`);
    expect(response.status).toBe(200);
    expect(response.body).toEqual({
        phish: true,
        meta: {
            url,
            id: expect.any(Number),
            verifiedAt: date.toISOString(),
            createdAt: expect.any(String),
            updatedAt: expect.any(String)
        }
    });
});
