# Is Phish?

## Getting Started

1. Download [docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/install/).
2. Run `docker-compose build`
3. Run `docker-compose up`
    * After running, seed db with `docker-compose exec web npm run seed`
4. By default, you can access the webapp at `http://localhost:3000`
5. Hitting [here](http://localhost:3000/verify?url=http%3A%2F%2Fwww.vantounoi.com%2Fupdate%2F) should result in a `200` with data.
6. Hitting [here](http://localhost:3000/verify?url=foo) should result in a `404`.

## Running Tests
1. Directly from the application
    * `npm test`
2. You can execute it through Docker
    * `docker-compose exec web npm test`

## High Level Decisions

There are a few ways to go about fulfilling the requirements of the Is Phish? service.

1. For every request, forward the request to PhishTank.
    * Unfortunately, PhishTank rate-limits 2000 requests/300 seconds, which is roughly 6.67 requests/second, well below the 50 requests/second that our application sees.
    * There's also the possibility/complexity of PhishTank being unreachable and needing to gracefully handle that.
2. For every request, forward the request to PhishTank and cache the response, keyed off the URL.
    * This may be feasible; without really knowing the domain too well, I can't say if this will alleviate the 50 req/s being too high. It's difficult to say if there are particularly "hot" URLs that will hit the cache frequently enough to make this work.
    * Slightly less risk with PhishTank being down, but again, depends on how often we get cache hits.
    * Increased complexity around cache invalidation and other rules (TTL, LRU vs LFU).
3. **Download the JSON provided by PhishTank and rely on it 100% as a source of truth.** <-- This is the option I went with
    * There's the possibility of the database entries becoming stale, but in my opinion the trade-off is worth it.
    * We can also mitigate the staleness by setting up a cron/script that periodically re-downloads the JSON from PhishTank; this warrants further investigation--I'm not sure how frequently PhishTank adds new entries.

## Technical Choices

I opted to play with some technologies I wasn't too familiar with. Chief among them was `docker` and `docker-compose`. It was actually surprisingly difficult to get them working the way I wanted. Even now, there are some hacks I'm less than pleased with.

I wanted an easy way to spin up a separate Docker container solely for integration testing; that proved to be not as straightforward as I had hoped. I followed the instructions [in the docs](https://docs.docker.com/compose/production/) but they don't actually provide an easy way to remove volume mounts between containers. I'm sure there's an easier way, I didn't find it in the allotted time for these exercises.

Some other technologies I chose to use:
* [sequelize](http://docs.sequelizejs.com/) as my ORM
    * Was pretty cool; I didn't use much of the powerful things like associations and such, but I could see how this would be nice to have.
* [TypeScript](https://www.typescriptlang.org/) is something I'm already familiar with using on the front end (I definitely love it there). I wanted to see how difficult it would be to set up on the back end. The current set-up isn't production-ready (`ts-node` is not meant to be used in prod). But from here it's fairly straightforward:
    1. I would just need to emit the TS -> JS and change where the `start` script points to.
    2. Deploy using something to manage clusters (`node-pm`, `pm2`, `cluster-service`).
    3. Deploy multiple instances and set up a proper load balancer (Nginx or HAProxy).
