export default {
    env: process.env.NODE_ENV || 'development',
    app: {
        port: process.env.CISCO_PORT || 3000
    }
};
