module.exports = {
    development: {
        username: process.env.DATABASE_USER || 'phisher',
        password: process.env.DATABASE_PASSWORD || 'tester',
        host: process.env.DATABASE_HOST || '127.0.0.1',
        database: 'phishing_service_development',
        dialect: 'mysql',
        logging: true
    },
    test: {
        username: 'phisher_test',
        password: 'tester',
        host: process.env.DATABASE_HOST || '127.0.0.1',
        database: 'phishing_service_test',
        dialect: 'mysql',
        logging: false
    }
};
