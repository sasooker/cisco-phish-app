'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addIndex('Phishes', {
      fields: ['url'],
      unique: true,
      name: 'unique__phishes__url'
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeIndex('Phishes', 'unique__phishes__url')
  }
};
