'use strict';

const {sequelize} = require('../../src/lib/db');
const json = require('../fixture/20180415183908-phish-tank');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return sequelize.models.Phishes.bulkCreate(
      json
        .filter(entry => entry.verified)
        .map((entry) => ({
          url: entry.url,
          createdAt: new Date(),
          updatedAt: new Date(),
          verifiedAt: new Date(entry.verification_time)
        })),
      {updateOnDuplicate: true});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Phishes', null, {});
  }
};
