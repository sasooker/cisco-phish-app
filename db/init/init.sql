CREATE USER 'phisher'@'%' IDENTIFIED BY 'tester';
CREATE DATABASE phishing_service_development;
GRANT ALL PRIVILEGES ON phishing_service_development.* TO phisher;

CREATE USER 'phisher_test'@'%' IDENTIFIED BY 'tester';
CREATE DATABASE phishing_service_test;
GRANT ALL PRIVILEGES ON phishing_service_test.* TO phisher_test;
