import './lib/db';
import * as Koa from 'koa';
import * as Router from 'koa-router';
import * as config from 'config';
import Phishes from './models/Phishes';

const app = new Koa();
const router = new Router();

router.get('/verify', async (ctx, next) => {
    const {query: {url}}: {query: {url: string}} = ctx;

    const maybeUrl = await Phishes.findOne({where: {url}});

    if (maybeUrl) {
        ctx.body = {
            meta: maybeUrl,
            phish: true
        };
    }

    next();
});

// Generic error handling
app.use(async (ctx, next) => {
    try {
        await next();

        if (!ctx.body || ctx.status === 404) {
            ctx.status = 404;
            ctx.body = {message: 'not found'};
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = config.get('env') === 'development'
            ? {message: err}
            : {message: 'internal server error'};
    }
});
app.use(router.routes());
app.use(router.allowedMethods());

export const server = app.listen(config.get('app.port'), () => console.log(`Application listening at ${config.get('app.port')}`));
