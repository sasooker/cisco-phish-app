const path = require('path');
const {Sequelize} = require('sequelize-typescript');
const dbConfig = require('../../../config/db-config');
const config = require('config');

const env = config.get('env');
const {
    username,
    password,
    database,
    host,
    dialect,
    logging
} = dbConfig[env];

module.exports.sequelize = new Sequelize({
    username,
    password,
    host,
    database,
    dialect,
    logging,
    modelPaths: [path.join(__dirname, '..', '..', 'models')]
});
