import {
    DataType,
    Table,
    Column,
    Model,
    CreatedAt,
    UpdatedAt,
    Unique,
    PrimaryKey
} from 'sequelize-typescript';

@Table
export default class Phishes extends Model<Phishes> {
    @PrimaryKey
    @Column
    id: number;

    @Unique
    @Column
    url: string;

    @Column(DataType.DATE)
    verifiedAt: Date;

    @CreatedAt
    createdAt: Date;

    @UpdatedAt
    updatedAt: Date;
}
